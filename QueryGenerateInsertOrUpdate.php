<?php

require_once 'QueryObject.php';

/**
 * Class gera values for insert or update
 * Lembrar que tem que colocar as chaves da tabela que recebera os valores
 * como indice ou chaves primarias para a validacao de insere ou atualizar funcionar 
 * corretamente
 */
class QueryGenerateInsertOrUpdate
{
    /**
     * Generates a QueryObject with the SQL query and the bindings.
     *
     * @param       $table
     * @param       $rows
     * @param array $exclude
     *
     * @return QueryObject
     */
    public function generate($table, $rows, array $exclude = [])
    {
        //$rows = $this->mountArray($rows);

        $columns = array_keys($rows[0]);
        $columnsString = implode('`,`', $columns);
        $values = $this->buildSQLValuesStringFrom($rows);
        $updates = $this->buildSQLUpdatesStringFrom($columns, $exclude);

        $query = vsprintf('insert into `%s` (`%s`) values %s on duplicate key update %s', [
            $table, $columnsString, $values, $updates,
        ]);

        return new QueryObject($query, $this->extractBindingsFrom($rows));
    }

    /**
     * Build the SQL "values()" string.
     *
     * @param $rows
     *
     * @return string
     */
    protected function buildSQLValuesStringFrom($rows)
    {
        return rtrim(array_reduce($rows, function ($values, $row) {
            return $values . '(' . rtrim(str_repeat('?,', count($row)), ',') . '),';
        }, ''), ',');
    }

    /**
     * Build the SQL "on duplicate key update" string.
     *
     * @param $rows
     * @param $exclude
     *
     * @return string
     */
    protected function buildSQLUpdatesStringFrom($rows, $exclude)
    {
        return trim(array_reduce(array_filter($rows, function ($column) use ($exclude) {
            return ! in_array($column, $exclude);
        }), function ($updates, $column) {
            return $updates . "`{$column}`=VALUES(`{$column}`),";
        }, ''), ',');
    }

    /**
     * extract the bindings.
     *
     * @param $rows
     *
     * @return mixed
     */
    protected function extractBindingsFrom($rows)
    {
        return array_reduce($rows, function ($result, $item) {
            return array_merge($result, array_values($item));
        }, []);
    }


    /**
     * Montei as chaves e valores aqui, mas podem ser enviadas 
     * e deixar os metodos montarem so
     */
    protected function mountArray(&$values) {
       // var_dump($values);
        $valuesAr = ['top' => 0, 'column' => 0,
        'sort_order' => 0, 'status' => 1, 'date_added' => date('Y-m-d'),
        'date_modified' => date('Y-m-d'), 'parentId' => 'Esse valor terei que fazer um select pra pegar'];

        $valuesArr = [];
        for ($i = 0; $i < count($values); $i++) {
            foreach($values[$i] as $key => $value){
                //echo "Valor ".$value;
                $valuesAr[$key] = $value;
    
                //parentId recebe o valor da ultima posicao do array que nao eh vazio
                //parentId nao pode ser assim, tem que pegar o categoryId da base
                if (!empty($value) && isset($value)) {
                    $valuesAr['parentId'] = $value;
                }
            }
            print_r($valuesAr);
            $valuesArr[] =  $valuesAr;
        }

        print_r($valuesArr);
        
        return [$valuesArr];
    }

}