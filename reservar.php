<?php

error_reporting(E_ERROR);
date_default_timezone_set('America/Recife');

class Reserva{

  var $validate = "";
  
  //Verificar se o imovel esta na tabela de inadimplentes
  function getInadiplenciaUsuario($user){
    $sql = "SELECT st_inadimplente FROM inadimplentes WHERE usuario = '$user'";
    $result = $this->getResultOnly($sql);
    if($result[0] == "N"){
      return false;
    }else{
      return true;
    }
  }
	
  //USADO PARA RETORNAR O CONJUNTO DE DADOS
  function getResultOnly($query){
    $connection = new Connection();
    $result = $connection->Query($query);
    $resultFinal = mysql_fetch_array($result);
    return $resultFinal;

  }
  
  //USADO PARA RETORNAR O CONJUNTO DE DADOS
  function getResultAll($query){
    $arrayData = array();
    $connection = new Connection();
    $result = $connection->Query($query);
   
    while($resultFinal = mysql_fetch_array($result)){
    	$arrayData[] = $resultFinal[0];
    	$j++;	
   }
   return $arrayData;

 }
  
 

  //FUNÇÃO PRINCIPAL, ELA QUE REALIZA OS TESTES!
  function makeBooking($login, $dataInicio, $dataFim, $hourStart, $hourEnd, $numero_imovel, $id_bloco, $id_areacomum, $id_area_pai){
    $hoje = date("Y-m-d");
    
    if($id_areacomum == 16){
       $hourEnd = "23:59";
    }
    
    //Na base a hora final estah 00:00
    if($id_areacomum == 40){
       $hourEnd = "24:00";
    }
	
    
    if((strtotime($dataInicio)) >= (strtotime($hoje)) && (strtotime($hourEnd)) > (strtotime($hourStart)) ){

      $resultInadiplencia = $this->getInadiplenciaUsuario($login);
      if(!$resultInadiplencia){
        $resultMoreBooking = $this->getMoreBookingOneDay($numero_imovel, $dataInicio, $id_bloco, $id_area_pai, $id_areacomum);
        if(!$resultMoreBooking){
            $resultUsedBookingOneDay = $this->getSameBookingOneDay($id_bloco, $numero_imovel, $dataInicio, $id_area_pai, $id_areacomum, $hourStart, $hourEnd);
            if(!$resultUsedBookingOneDay){
            $resultSameBookingOneWeek = $this->getSameBookingOneWeek($id_bloco, $numero_imovel, $dataInicio, $id_area_pai, $id_areacomum);
            if(!$resultSameBookingOneWeek){
              $resultSameBookingOneMonth = $this->getSameBookingOneMonth($id_bloco, $numero_imovel, $dataInicio, $id_area_pai, $id_areacomum);
              if(!$resultSameBookingOneMonth){
                $resultCheckDayAntecipation = $this->checkDayAntecipation($dataInicio, $id_area_pai, $id_areacomum);
                if(!$resultCheckDayAntecipation){
                  $resultCheckSameDayHour = $this->checkDateSameDay($id_areacomum, $dataInicio, $hourStart, $hourEnd);
                  if(!$resultCheckSameDayHour){
                    $resultCheckTimeForArea = $this->checkTimeForArea($id_areacomum, $hourStart, $hourEnd, $dataInicio, $id_area_pai);
                    if(!$resultCheckTimeForArea){
                      $resultCheckImmobileBlock = $this->checkImmobileBlock($numero_imovel, $id_bloco, $dataInicio);
                      if(!$resultCheckImmobileBlock){
                          $resultTeste = $this->checkAmountRes($id_areacomum, $dataInicio, $id_bloco, $numero_imovel, $hourStart, $hourEnd, $id_area_pai);
                         if(!$resultTeste){
                          $resultInsertBooking = $this->finalInsertBooking($dataInicio, $hourStart, $hourEnd, $numero_imovel, $id_bloco, $id_areacomum,
                        "", $login, $id_area_pai);
                            if(!$resultInsertBooking){
                               $resultado[] = array("error" => false, "description" => "Reserva solicitada com sucesso.");
                            
                            }else{
                             $resultado[] = array("error" => true, "description" => "Não foi possível reservar neste momento.");
                            }
                            
                            }else{
                            $resultado[] = array("error" => true, "description" => $GLOBALS['validateHolidayEve']);
                          }
    
                            }else{
                            $resultado[] = array("error" => true, "description" => "Não foi possível realizar a reserva nesse momento.");
                          }
                       
                          }else{
                               $resultado[] = array("error" => true, "description" => $GLOBALS['validateTimeForArea']);
                          }
                          
                    }else{
                  $resultado[] = array("error" => true, "description" => "Neste horário a área está reservada. Por favor, escolha outro horário.");
                }
              }else{
                $resultado[] = array("error" => true, "description" => "Não é possível realizar uma reserva com tantos dias de antecedência! Por favor escolha outra data.");
              }
              //$resultado[] = array("error" => false, "description: " => "");
            }else{
              $resultado[] = array("error" => true, "description" => "Desculpe, você já atingiu a quota de reservas do mês para esta área.");
            }
          
            }else{
              $resultado[] = array("error" => true, "description" => $GLOBALS['messageValidate']);
            }
            
            }else{
                  $resultado[] = array("error" => true, "description" => $GLOBALS['messageValidateDay']);
                } 
          }else{
            $resultado[] = array("error" => true, "description" => "Desculpe, você não pode fazer essa reserva, pois já fez outra para este dia. Escolha outra data por favor.");
          }
        }else{
          $resultado[] = array("error" => true, "description" => "Desculpe, restrições administrativas impedem sua reserva. Por favor, entre em contato com o síndico.");
        }
      }else{
      	
      	  $resultado[] = array("error" => true, "description" => "Data e/ou hora da solicitação inválida. Verifique e solicite novamente!");
        
      }
    return json_encode($resultado);
  }


  //Verificar se é possível fazer mais de uma reserva no mesmo dia:
  function getMoreBookingOneDay($id_numero_imovel, $dt_data, $id_bloco, $id_area_pai, $id_cadastro_reserva_area_comum){
    $sqlReservaDiaConfiguracao = "SELECT perm_varias_reserva_dia FROM cadastro_area_comum WHERE id_cadastro_reserva_area_comum = $id_cadastro_reserva_area_comum";
    $resultReservaDiaConfiguracao = $this->getResultOnly($sqlReservaDiaConfiguracao);
    
    if($resultReservaDiaConfiguracao[0] == "N"){
      $sqlVerifyBookingInDate = "SELECT count(idevento) FROM reserva_area_comum WHERE (id_bloco = $id_bloco and
        id_numero_imovel = $id_numero_imovel and dt_data = '$dt_data' and status <> 'cancelado' and id_area_pai = $id_area_pai)";
      $resultVerifyBookingInDate = $this->getResultOnly($sqlVerifyBookingInDate);
      
      return $resultVerifyBookingInDate[0] == 0 ? false : true;
    }else{
      return false;
    }
  }

  //Verificar quantas vezes o imovel fez reserva da mesma area comum no mês
  function getSameBookingOneMonth($id_bloco, $id_numero_imovel, $dt_data, $id_area_pai, $id_cadastro_reserva_area_comum){

     $sqlCheck = "SELECT ignora_qtd_reserva FROM cadastro_area_comum WHERE id_cadastro_reserva_area_comum = $id_cadastro_reserva_area_comum";
     $resultValue = $this->getResultOnly($sqlCheck);
   if(!empty($resultValue[0]) and isset($resultValue[0]) and $resultValue[0] == "N") {

      $mes_reserva = date('m', strtotime($dt_data));
      $ano_reserva = date('Y', strtotime($dt_data));
      $sqlQtyBooking = "SELECT qtd_reserva_mes FROM cadastro_area_comum WHERE id_cadastro_reserva_area_comum = $id_cadastro_reserva_area_comum";
      $resultQtyBooking = $this->getResultOnly($sqlQtyBooking);
      $sqlSameBookingOneMonth = "SELECT count(idevento) FROM reserva_area_comum
      WHERE id_area_pai = $id_area_pai AND (id_bloco = $id_bloco AND id_numero_imovel = $id_numero_imovel) AND
      (MONTH(dt_data) = '$mes_reserva' AND YEAR(dt_data) = '$ano_reserva') AND status <> 'cancelado'";
      $resultSameBookingOneMonth = $this->getResultOnly($sqlSameBookingOneMonth);
      
      return $resultSameBookingOneMonth[0] >= $resultQtyBooking[0] ? true : false;
    }
  }
  
  //Verificar quantas vezes o imovel fez reserva da mesma area comum na semana
  function getSameBookingOneWeek($id_bloco, $id_numero_imovel, $dt_data, $id_area_pai, $id_cadastro_reserva_area_comum){
      if($id_area_pai <> 1) {
          $GLOBALS['messageValidate'] = "";
          $mes_reserva = date('m', strtotime($dt_data));
          $ano_reserva = date('Y', strtotime($dt_data));
          $semana_reserva = date('W', strtotime($dt_data));
          if($id_area_pai == 11){//CAMPO DE FUTEBOL
          
            //Verifica se o campo de futebol jah foi reservado 3 vezes na semana independente do imovel
            $sqlSameBookingOneWeekBigger3 = "SELECT count(idevento) FROM reserva_area_comum
            WHERE id_cadastro_reserva_area_comum = $id_cadastro_reserva_area_comum AND (id_bloco = $id_bloco AND id_numero_imovel = $id_numero_imovel AND
           (MONTH(dt_data) = '$mes_reserva' AND YEAR(dt_data) = '$ano_reserva' AND WEEKOFYEAR(dt_data) = '$semana_reserva') AND status <> 'cancelado'";
           
          
           $resultSameBookingOneBigger3 = $this->getResultOnly($sqlSameBookingOneWeekBigger3);
           
           //Verifica quantas vezes o imovel reservou o campo de futebol na semana
           $sqlSameBookingOneWeek = "SELECT count(idevento) FROM reserva_area_comum
            WHERE id_cadastro_reserva_area_comum = $id_cadastro_reserva_area_comum AND (id_bloco = $id_bloco AND id_numero_imovel = $id_numero_imovel) AND
           (MONTH(dt_data) = '$mes_reserva' AND YEAR(dt_data) = '$ano_reserva' AND WEEKOFYEAR(dt_data) = '$semana_reserva') AND status <> 'cancelado' AND status <> 'bloqueado'";
           
          
           $resultSameBookingOneWeek = $this->getResultOnly($sqlSameBookingOneWeek);
          
            if($resultSameBookingOneBigger3[0] >= 3) {
                $GLOBALS['messageValidate'] = "Desculpe, essa área só pode ser reservada 3 vezes por semana.";
            }
            
            if ($resultSameBookingOneWeek[0] >= 1) {
                $GLOBALS['messageValidate'] = "Desculpe, você só pode reservar essa área 1 vez por semana.";
            }
            
            if(!empty($GLOBALS['messageValidate'])){
                return true;
            }else {
                return false;
            }
          
        //   return $resultSameBookingOneBigger3[0] >= 3 or $resultSameBookingOneWeek[0] >= 1 ? true : false;   
         } 
         
         if ($id_area_pai != 15) {
             
             $sqlSameBookingOneMonth = "SELECT count(idevento) FROM reserva_area_comum
            WHERE id_cadastro_reserva_area_comum = $id_cadastro_reserva_area_comum AND (id_bloco = $id_bloco AND id_numero_imovel = $id_numero_imovel) AND
           (MONTH(dt_data) = '$mes_reserva' AND YEAR(dt_data) = '$ano_reserva' AND WEEKOFYEAR(dt_data) = '$semana_reserva') AND status <> 'cancelado'";
    
          $resultSameBookingOneMonth = $this->getResultOnly($sqlSameBookingOneMonth);
          
          if ($resultSameBookingOneMonth[0] >= 1) {
                $GLOBALS['messageValidate'] = "Desculpe, você só pode reservar essa área 1 vez por semana.";
          }
         }
            
          
          if(!empty($GLOBALS['messageValidate'])){
                return true;
          }else {
            return false;
          }
            
      }else{
          return false;
      }
      
  }
  
  
  //Verificar se o imovel ja fez reserva da area comum academia no dia ou se tem reserva que ainda vai usar
  function getSameBookingOneDay($id_bloco, $id_numero_imovel, $dt_data, $id_area_pai, $id_cadastro_reserva_area_comum, $hourStart, $hourEnd){
      
      //Se for a area comum academia
      if($id_area_pai == 14 || $id_area_pai == 15) {
          
          $GLOBALS['messageValidateDay'] = "";
          $dia_reserva = date('d', strtotime($dt_data));
          $mes_reserva = date('m', strtotime($dt_data));
          $ano_reserva = date('Y', strtotime($dt_data));
          
         
        
            //Verifica se o imovel ja tem uma reserva da area academia no dia
            $sqlSameBookingOneDay = "SELECT count(idevento) FROM reserva_area_comum
                                        WHERE id_area_pai = $id_area_pai AND (id_bloco = $id_bloco AND id_numero_imovel = $id_numero_imovel) AND
                                       (dt_data = '$dt_data') AND status <> 'cancelado'";
       
          
           $resultSameBookingOneDay = $this->getResultOnly($sqlSameBookingOneDay);
           
           //Verifica se o imovel jah utilizou a ultima reserva da area comum academia
           $sqlCheckUsedBookingAcademy = "SELECT count(idevento) FROM reserva_area_comum
                                            WHERE id_area_pai = $id_area_pai AND (id_bloco = $id_bloco AND id_numero_imovel = $id_numero_imovel)
                                            AND (CONCAT(dt_data, ' ', dt_hora_inicio) >=  CURRENT_TIMESTAMP  AND status <> 'cancelado')";
                                           
          
           $resultCheckUsedBookingAcademy = $this->getResultOnly($sqlCheckUsedBookingAcademy);
          
            if($resultSameBookingOneDay[0] > 0) {
               
                $GLOBALS['messageValidateDay'] = "Desculpe, você só pode reservar essa área 1 vez por dia.";
            }
            
            if ($resultCheckUsedBookingAcademy[0] > 0) {
                $GLOBALS['messageValidateDay'] = "Desculpe, você só pode reservar essa área depois que usar a sua outra reserva.";
            }
            
            
            $expHourStart = explode(':', $hourStart);
            
            $expHourEnd = explode(':', $hourEnd);
            
            
            
            if ($expHourStart[1] > 0) {
                $GLOBALS['messageValidateDay'] = "Desculpe, você precisa informar um horário fechado. Exemplo: 07:00 até 08:00";
            }
            
            if ($expHourEnd[1] > 0) {
                $GLOBALS['messageValidateDay'] = "Desculpe, você precisa informar um horário fechado. Exemplo: 07:00 até 08:00";
            }
            
            
            if(!empty($GLOBALS['messageValidateDay'])){
                return true;
            }else {
                return false;
            }
           
      }else{
          return false;
      }
      
  }

/*MUDEI ESSA FUNCAO*/
//Verificando se o usuário está tentando reservar uma área antes do período permitido.
  function checkDayAntecipation($dt_data, $id_area_pai, $id_cadastro_reserva_area_comum){
    //Buscando na base de dados a quantidade de dias permitidos pra reservar uma área comum	por antecedência.
    $sqlQtdyDayAntecipation = "SELECT nu_antecedencia_max FROM cadastro_area_comum WHERE id_cadastro_reserva_area_comum = $id_cadastro_reserva_area_comum";
   
    $resultQtdyDayAntecipation = $this->getResultOnly($sqlQtdyDayAntecipation);
    
    $current_date = date('Y-m-d');
    $startTimeStamp = strtotime("$dt_data");
    $endTimeStamp = strtotime($current_date);
    $timeDiff = abs($endTimeStamp - $startTimeStamp);
   
    $numberDays = $timeDiff/86400;

    
    if($numberDays > $resultQtdyDayAntecipation[0]){
      return true;
    }else{
      return false;
    }
  }

  function checkDateSameDay($id_area_comum, $dt_data, $hourStart, $hourEnd, $id_area_pai){
    $uHourStart = date ('H:i',strtotime($hourStart));
    $uHourEnd = date ('H:i',strtotime($hourEnd));
    
    if($id_area_pai <> 11){
        $sqlCheckDay = "select r.dt_data, COUNT(*)
    					from reserva_area_comum r
    					where (r.id_cadastro_reserva_area_comum = '$id_area_comum' AND
    					r.dt_data = '$dt_data' AND
    					(r.dt_hora_inicio <= '$uHourStart' AND r.dt_hora_fim >= '$uHourStart' OR
    					r.dt_hora_inicio <= '$uHourEnd' AND r.dt_hora_fim >= '$uHourEnd' OR
    					r.dt_hora_inicio >= '$uHourStart' AND r.dt_hora_fim <= '$uHourEnd' OR
    					r.dt_hora_inicio <= '$uHourStart' AND r.dt_hora_fim >= '$uHourEnd')) AND
    					(r.status = 'confirmado' OR r.status = 'pendente' OR r.status = 'bloqueado')";
    					
    					
    					echo $sqlCheckDay;
    			
    				
        $resultCheckDay = $this->getResultOnly($sqlCheckDay);
        
    
        if($resultCheckDay[0] != null || !empty($resultCheckDay[0])){
          return true;
        }else{
          return false;
        }
}
  }

#########################################################################################################################################
  //IMPLEMENTANDO NOVAS REGRAS DE MAX
  //VERIFICANDO A QUANTIDADE DE RESERVAS NA SEXTA, SABADO E FERIADO
  function checkAmountRes($id_cadastro_reserva_area_comum, $dt_data, $id_bloco, $id_numero_imovel, $hourStart, $hourEnd, $id_area_pai){
   $GLOBALS['validateHolidayEve'] = "";
    //PEGANDO O NUMERO DO DIA DA SEMANA
    $dia_semana = date("N", strtotime($dt_data));

    //VERIFICANDO SE A AREA COMUM E O SALAO DE FESTAS
    if ($id_cadastro_reserva_area_comum == 16) {
      //Verificando se o dia da semana é sexta ou sabado
      if(($dia_semana >= 5) and ($dia_semana <= 6)){
        $validar_dia = 1;
      }else{
        $validar_dia = 0;
      }

      //SOMANDO  + UM DIA A DATA PARA VERIFICAR SE O DIA ESCOLHIDO É VÉSPERA DE FERIADO
     $NewData = date('Y-m-d', strtotime("+1 days", strtotime($dt_data)));
      
      $sqlCheckHolidayEve = "SELECT id_feriados FROM feriados WHERE dt_data = '$NewData'";
     
      $resultCheckHolidayEve = $this->getResultOnly($sqlCheckHolidayEve);
    
      if (!empty($resultCheckHolidayEve) and isset($resultCheckHolidayEve[0])) {
          $validar_dia = 1;
      }
    
      //Validando número de reservas já feitas pelo usuário no salão de festas, nos dias: sexta, sábado e véspera de feriado
      if(($validar_dia == 1) and ($id_cadastro_reserva_area_comum == 16)){   
      //echo "Entrei no if pra saber q eh sexta ";
          $sqlCheckAmountRes = "SELECT dt_data FROM reserva_area_comum 
                   WHERE id_bloco = $id_bloco  and id_numero_imovel = $id_numero_imovel
                  and id_cadastro_reserva_area_comum = $id_cadastro_reserva_area_comum and (status <> 'cancelado' or status <> 'bloqueado') and dt_data >= CURDATE()";
	                       
        $resultCheckAmountRes = $this->getResultAll($sqlCheckAmountRes);
      
      		 
        if (!empty($resultCheckAmountRes[0]) and isset($resultCheckAmountRes[0])){
          $count_itens = count($resultCheckAmountRes, COUNT_RECURSIVE);
          $count_fds = 0;
          for($i = 0; $i < $count_itens; $i++){
         
            $dia_reser = date("N", strtotime($resultCheckAmountRes[$i]));
                     
            if(($dia_reser >= 5) and ($dia_reser <= 6)){
             $count_fds++;
            
            }else{
            
            $NewData2 = date('Y-m-d', strtotime("+1 days", strtotime($resultCheckAmountRes[$i])));
      
              $sqlCheckHolidayEve2 = "SELECT id_feriados FROM feriados WHERE dt_data = '$NewData2'"; 
            
              $resultCheckHolidayEve2 = $this->getResultOnly($sqlCheckHolidayEve2);
             
      
                if (!empty($resultCheckHolidayEve2[0]) and isset($resultCheckHolidayEve2[0])){
            
                  $count_fds++;
                }
            }
          }
          
          if($count_fds >= 2){
          
            $GLOBALS['validateHolidayEve'] = "O limite de reservas foi excedido! Para sextas-feiras, sábados ou vésperas de feriado.";
          }
          if(empty($GLOBALS['validateHolidayEve'])){
      	      return false;
  	  }else{
       	      return true;
 	}
         
   	 }
      }
      
  }
  
   $dia_semana = date("N", strtotime($dt_data));
   //VERIFICANDO SE A AREA COMUM É O ESPACO GOURMET
  if($id_cadastro_reserva_area_comum == 17){
	
      //PEGANDO A HORA DE INICIO E FIM DA RESERVA CADASTRADA
      $checkHourReserve = "SELECT dt_hora_inicio, dt_hora_fim from reserva_area_comum WHERE dt_data = '$dt_data' and id_cadastro_reserva_area_comum = $id_cadastro_reserva_area_comum";
      
      $resultCheckHourReserve = $this->getResultOnly($checkHourReserve);

      //HORA DE INICIO QUE A AREA FOI RESERVADA
      $horaInicioRes = date('H:i:s',strtotime($resultCheckHourReserve[0]));
      
      //HORA DE FIM QUE A AREA FOI RESERVADA
      $horaFimRes = date ('H:i:s',strtotime($resultCheckHourReserve[1]));
	
      //HORA DE INICIO QUE O USUARIO ESTA QUERENDO RESERVAR
      $horaInicio = date('H:i:s',strtotime($hourStart));
      //HORA DE FIM QUE O USUARIO ESTA QUERENDO RESERVAR
      $horaFim = date ('H:i:s',strtotime($hourEnd));
	
	
      /**
      *TESTANDO SE A HORA DE INICIO DA RESERVA QUE JA EXISTE E MAIOR QUE A QUE O 
      *USUARIO ESTA SOLICITANDO E  SE A HORA DO FIM QUE O USUARIO ESTA SOLICITANDO
      *E MAIOR QUE A HORA DO INICIO QUE E PERMITIDO DE ACORDO COM O DIA DA SEMANA
      */
      
      //PEGANDO A HORA DE INICIO E FIM QUE PODE SER RESERVADA A AREA COMUM
      $checkHourStartEnd = "SELECT hr_inicio, hr_fim FROM dia_reserva_area_comum WHERE num_dia = '$dia_semana' and id_area_comum = $id_cadastro_reserva_area_comum'";
      $resultCheckHourStartEnd = $this->getResultOnly($checkHourStartEnd);
      //HORA DO INICIO DE ACORDO COM O DIA SOLICITADO
      $horaStartAllowed = date ('H:i:s',strtotime($resultCheckHourStartEnd[1]));
	
	 $diffHour = abs(strtotime($horaFimRes)-(strtotime($horaInicio)))/3600;
        
      if ($horaInicioRes > $horaFim and $horaFim > $horaStartAllowed) {
          
         
          
           //CALCULANDO A DIFERENCA DE HORAs
           "Diferenca de horas ".$diffHour = abs(strtotime($horaFimRes)-(strtotime($horaInicio)))/3600;
           #VERIFICANDO SE O INTERVALO E MENOR QUE 4 HORAS
          if($diffHour < 4 ){						
		$GLOBALS['validateHolidayEve'] = "Atenção: Já existe uma reserva para esse dia, com o seguinte horário: ".$horaInicioRes." às ".$horaFimRes;	
	  }		
	if(empty($GLOBALS['validateHolidayEve'])){
      	      return false;
  	}else{
       	      return true;
 	}

      #VERIFICANDO SE O INTERVALO E MENOR QUE 4 HORAS
      }else{
      	if($diffHour < 4 ){						
		$GLOBALS['validateHolidayEve'] = "Atenção: Já existe uma reserva para esse dia, com o seguinte horário: ".$horaInicioRes." às ".$horaFimRes;	
	  }	
      	if(empty($GLOBALS['validateHolidayEve'])){
      	      return false;
  	  }else{
       	      return true;
 	}
      }

  }//FIM DO TESTE DA AREA 17 ESPACO GOURMET
  
   //VERIFICANDO SE A ANTECEDENCIA DA SOLICITACAO DA RESERVA DA SAUNA E MENOR QUE 1 HORA
   if($id_area_pai == 5){
   date_default_timezone_set('America/Recife');
    //DATA SOLICITADA PRA RESERVAR
    $dateReserve = ($dt_data);
   
    $horaTeste = $hourStart;
    $dt_hr_reserva = $dateReserve." ".$horaTeste;
    
    $dataCurrente = date("Y-m-d H:i");
    
    $intervalo = abs(strtotime($dt_hr_reserva) - (strtotime($dataCurrente)))/3600;
   
    if($intervalo < 1){
       $GLOBALS['validateHolidayEve'] = "Atenção: Horário Indisponível! A reserva não pode ser realizada com menos de 1(uma hora) de antecedência";
    }
    if(empty($GLOBALS['validateHolidayEve'])){
         return false;
    }else{
         return true;
    }
    
  }
  
  //VERIFICANDO SE A AREA PAI E 4 COMPLEXO ESPORTIVO(QUADRA DE VOLEI DE AREIA, QUADRA DE TENIS, QUADRA POLIESPORTIVA, CAMPO DE FUTEBOL)
  if ($id_area_pai == 11) {
      
      //ADD POR CAUSA DA REGRA DO COVID
      /*$checkCountReserveIdParentArea11 = "SELECT COUNT(idevento) from reserva_area_comum WHERE 
								    id_area_pai = 11 and id_bloco = '{id_bloco}' and id_numero_imovel = '{id_numero_imovel}'
                                    and status = 'pendente'";
                                    
      $resultcheckCountReserveIdParentArea11 = $this->getResultOnly($checkCountReserveIdParentArea11);     
      
      if (!empty($resultcheckCountReserveIdParentArea11[0]) && $resultcheckCountReserveIdParentArea11[0] > 0) {
          $GLOBALS['validateHolidayEve'] = "Atenção: Você só pode efetuar uma reserva, depois que a sua reserva que está pendente for atualizada.";
      }*/
                                    
  
      //PEGANDO O NUMERO DA SEMANA E O ANO
      $yearWeek = date("oW", strtotime($dt_data));
	//VERIFICANDO SE NÃO EXISTE RESERVA NO HORARIO SOLICITADO PELO USUARIO
       $uHourStart = date ('H:i',strtotime($hourStart));
    	$uHourEnd = date ('H:i',strtotime($hourEnd));
  	$checkReserveAreaFour = "select r.dt_data, r.dt_hora_inicio, r.dt_hora_fim
    					from reserva_area_comum r
    					where (r.id_cadastro_reserva_area_comum = '$id_cadastro_reserva_area_comum' AND
    					r.dt_data = '$dt_data' AND
    					(r.dt_hora_inicio <= '$uHourStart' AND r.dt_hora_fim > '$uHourStart' OR
    					r.dt_hora_inicio < '$uHourEnd' AND r.dt_hora_fim >= '$uHourEnd' OR
    					r.dt_hora_inicio >= '$uHourStart' AND r.dt_hora_fim <= '$uHourEnd' OR
    					r.dt_hora_inicio <= '$uHourStart' AND r.dt_hora_fim >= '$uHourEnd')) AND
    					(r.status = 'confirmado' OR r.status = 'pendente' OR r.status = 'bloqueado')";
			  
 $resultCheckReserveAreaFour = $this->getResultOnly($checkReserveAreaFour); 
				
	if (!empty($resultCheckReserveAreaFour[0]) and $resultCheckReserveAreaFour[0] <> null) {
		$GLOBALS['validateHolidayEve'] = "Atenção: Horário Indisponível! Já existe uma reserva para o seguinte horário. Início: ".$resultCheckReserveAreaFour[1]." Fim: ".$resultCheckReserveAreaFour[2];
	
	 if(empty($GLOBALS['validateHolidayEve'])){
            return false;
          }else{
            return true;
          }

        }



      /**VERIFICANDO SE A AREA SOLICITADA E O CAMPO DE FUTEBOL
      *  SE FOR O CAMPO DE FUTEBOL EU TENHO QUE VER QUANTAS VEZES A 
      *  AREA FOI RESERVADA POR SEMANA INDEPENTE DE QUEM RESERVOU
      */
      if($id_area_pai == 11){
        //VERIFICANDO A QUANTIDADE DE RESERVA FEITA PELO IMOVEL SOLICITANTE DO CAMPO DE FUTEBOL
         $checkReserveFootballField = "SELECT COUNT(*) from reserva_area_comum WHERE  DATE_FORMAT(dt_data,'%x%v') = '$yearWeek' and id_cadastro_reserva_area_comum = $id_cadastro_reserva_area_comum and id_bloco = $id_bloco and id_numero_imovel = $id_numero_imovel";
         
         $resultCheckReserveFootballField = $this->getResultOnly($checkReserveFootballField);
        
         if (!empty($resultCheckReserveFootballField[0]) and isset($resultCheckReserveFootballField[0])) {
            $GLOBALS['validateHolidayEve'] = "Atenção: Limite de Reservas da Semana Excedido! Favor escolher outra data.";
         }
         if(empty($GLOBALS['validateHolidayEve'])){
           return false;
         }else{
            return true;
         }

      }else{
     
        //VERIFICANDO QUANTAS VEZES O IMOVEL RESERVOU O COMPLEXO ESPORTIVO
        $checkCountReserveSportsComplex = "SELECT COUNT(*) from reserva_area_comum WHERE  DATE_FORMAT(dt_data,'%x%v') = '$yearWeek' and id_cadastro_reserva_area_comum <> '18' and id_area_pai = '4'  and id_bloco = $id_bloco and id_numero_imovel = $id_numero_imovel";
      // echo "Entrei no else que nao eh area 18";
       
         $resultCheckCountReserveSportsComplex = $this->getResultOnly($checkCountReserveSportsComplex); 
         
        if(isset($resultCheckCountReserveSportsComplex[0]) and $resultCheckCountReserveSportsComplex[0] >= 3){
            $GLOBALS['validateHolidayEve'] = "Atenção: Limite de Reservas da Semana Excedido! Favor escolher outra data.";
        }
        if(empty($GLOBALS['validateHolidayEve'])){
          return false;
        }else{
          return true;
        }
      }//FIM DO TESTE DA AREA 18

      //VERIFICANDO A QUANTIDADE DE RESERVAS PERMITIDA POR SEMANA
      $reserveAllowedWeek = "SELECT qtd_reserva_semana FROM cadastro_area_comum WHERE id_cadastro_reserva_area_comum = $id_cadastro_reserva_area_comum";
      $resultCheckReserveAllowedWeek = $this->getResultOnly($reserveAllowedWeek); 

      if (!empty($resultCheckReserveAllowedWeek[0]) and isset($resultCheckReserveAllowedWeek[0])) {
        $countAllowed = $resultCheckReserveAllowedWeek[0];    
    	//echo "Qtde reservas permitida ".$countAllowed[0];
        $checkCountReserveArea = "SELECT COUNT(*) from reserva_area_comum WHERE  DATE_FORMAT(dt_data,'%x%v') = '$yearWeek' and id_cadastro_reserva_area_comum = $id_cadastro_reserva_area_comum";
        $resultCheckCountReserveArea = $this->getResultOnly($checkCountReserveArea); 
        
        if(!empty($resultCheckCountReserveArea[0]) and isset($resultCheckCountReserveArea[0])){
          $countReserve = $resultCheckCountReserveArea[0];
         
          if($countReserve >= $countAllowed){          
            $GLOBALS['validateHolidayEve'] = "Atenção: Limite de Reservas Excedido! Favor escolher outra data.";
          }
          if(empty($GLOBALS['validateHolidayEve'])){
            return false;
          }else{
            return true;
          }
          
        }
      }//FIM DO TESTE DA QUANTIDADE DE RESERVAS POR SEMANA
      
      //VERIFICANDO SE A ANTECEDENCIA DA SOLICITACAO DA RESERVA DA SAUNA E MENOR QUE 1 HORA
  
      
  }//FIM DO TESTE SE EH AREA PAI 4(COMPLEXO ESPORTIVO)
  	
  
  
}

#########################################################################################################################################
  /*MUDEI ESSA FUNCAO*/
  //SERVE PARA INSERIR DE FATO, RESPEITAR OS PADRÕES DE HORA
  function finalInsertBooking($dt_data, $hourStart, $hourEnd, $id_numero_imovel, $id_bloco, $id_cadastro_reserva_area_comum,
  $de_observacao, $login, $id_area_pai){

    //$dt_data = '2015-09-25';
    //$hourStart = '08:00';
    $horaInicio = date('H:i:s',strtotime($hourStart));
    //$hourEnd = '23:59';
    $horaFim = date ('H:i:s',strtotime($hourEnd));
   
    $sqlValue = "SELECT nu_valor, qtd_reserva_mes_gratis, qtd_reserva_ano_gratis FROM cadastro_area_comum WHERE id_cadastro_reserva_area_comum = $id_cadastro_reserva_area_comum LIMIT 1";

    
    $resultValue = $this->getResultOnly($sqlValue);

    
    $value = empty($resultValue[0]) ? 0.00 : $resultValue[0];
    //QTDE MES GRATIS
    $valueGratisMes = ($resultValue[1]);
    //QTDE ANO GRATIS
    $valueGratisAno = ($resultValue[2]);
    
    
   
    $sqlTipoArea = "SELECT id_tipo_area FROM cadastro_area_comum WHERE id_cadastro_reserva_area_comum = $id_cadastro_reserva_area_comum";
    $tipoArea = $this->getResultOnly($sqlTipoArea);
    $codTipoArea = $tipoArea[0];


    //VERIFICANDO SE A QTDE DE RESERVA GRATIS NO MES E MAIOR QUE 0
    if($valueGratisMes > 0){
      $qtdeGratisMes = "SELECT count(idevento) FROM reserva_area_comum WHERE (id_bloco = $id_bloco 
                        AND id_numero_imovel = $id_numero_imovel) AND MONTH(dt_data) = MONTH('$dt_data') 
                        AND YEAR(dt_data) = YEAR('$dt_data') 
                        AND id_tipo_area = $codTipoArea
                        AND status <> 'cancelado'"; 
      $resultQtdMes = $this->getResultOnly($qtdeGratisMes);
      //QUANTIDADE DE RESERVA(SE A QTD DE RESERVA FEITA PELO IMOVEL FOR MAIOR QUE A PERMITIDA NO MES)
      $valor = ($resultQtdMes[0] >= $valueGratisMes) ? $value : 0.00;
    
    //Testando se a quantidade de reserva(s) no ano é maior que 0
    }elseif($valueGratisAno > 0){
      $qtdReservaAno = "SELECT count(idevento) FROM reserva_area_comum WHERE (id_bloco = $id_bloco 
                   AND id_numero_imovel = $id_numero_imovel) AND YEAR(dt_data) = YEAR('$dt_data')
                   AND id_tipo_area = $codTipoArea
                   AND status <> 'cancelado'"; 
      

      $resultQtdAno = $this->getResultOnly($qtdReservaAno);
      //QUANTIDADE DE RESERVA(SE A QTD DE RESERVA FEITA PELO IMOVEL FOR MAIOR QUE A PERMITIDA NO ANO)
      $valor = ($resultQtdAno[0] >= $valueGratisAno) ? $value : 0.00;

    }else{
      $valor = $value;
    }

   //COLOQUEI ISSO AQUI
    $sqlValueHorario = "SELECT st_horario_sn FROM cadastro_area_comum WHERE id_cadastro_reserva_area_comum = $id_cadastro_reserva_area_comum LIMIT 1";
    $resultValueHorario = $this->getResultOnly($sqlValueHorario);



    //PEGANDO A HORA DE INICIO E FIM DA ÁREA COMUM
    $sqlValue = "SELECT hr_inicio, hr_fim, tmp_duracao FROM dia_reserva_area_comum WHERE id_area_comum = $id_cadastro_reserva_area_comum AND num_dia = (SELECT DAYOFWEEK('$dt_data')) LIMIT 1";
    $resultValue = $this->getResultOnly($sqlValue);
    
    //Verificacao se pode mudar a data, se nao for possivel eh setado o valor do banco
    if ($resultValueHorario [0] == 'N') {
        $horaInicio = $resultValue[0];
        $horaFim = $resultValue[1];
    }

   
         $sqlInsert = mysql_query("INSERT INTO reserva_area_comum(dt_data,dt_hora_inicio,dt_hora_fim,
                               id_numero_imovel,id_bloco,id_cadastro_reserva_area_comum, id_area_pai, id_tipo_area, nu_valor,de_observacao,status, dt_hr_solicitacao, login) 
                               VALUES ('$dt_data', '$horaInicio','$horaFim', '$id_numero_imovel', '$id_bloco', $id_cadastro_reserva_area_comum, $id_area_pai, $codTipoArea, $valor, '$de_observacao', 'confirmado', now(), '$login')");
    
     

    if (!$sqlInsert) {
        return true;
    } else {
        
        
            //BUSCANDO O ULTIMO ID INSERIDO DE RESERVA DO IMOVEL
            $sqlValueId = "SELECT idevento FROM reserva_area_comum WHERE login = '$login' ORDER BY idevento DESC LIMIT 1";
            
             $resultValueId = $this->getResultOnly($sqlValueId);
             
            $dt_solicitacao = date('Y-m-d H:i');
          $sqlInsertLog = mysql_query("INSERT INTO sc_log(inserted_date,username,application, creator,action,description) VALUES (now(), '$login','reserva_app', 'app', 'insert', 
                                  '--> keys <--idevento : $resultValueId[0]|| --> fields <-- dt_data (new)  : $dt_data||dt_hora_inicio (new)  : $horaInicio||dt_hora_fim (new)  : $horaFim||id_numero_imovel (new)  : $id_numero_imovel||id_bloco (new)  : $id_bloco||id_cadastro_reserva_area_comum (new)  : $id_cadastro_reserva_area_comum||id_area_pai (new)  : $id_area_pai||nu_valor (new)  : $valor||de_observacao (new)  : $de_observacao||status (new)  : pendente||login (new)  : $login||dt_hr_solicitacao (new)  : $dt_solicitacao')");
                                   
            //DADOS CONDOMINIO
            $dadosCondominio = $this->getDadosCondominioSql();
            $emailCondominio = $dadosCondominio[0];
               
            $data = date('d-m-Y', strtotime($dt_data));
            $nome_area = $this->getNomeArea($id_cadastro_reserva_area_comum);  
            
             //ENVIAR EMAIL
            include_once 'enviaEmail.php';
            enviarEmail($data, $nome_area, $id_numero_imovel, $emailCondominio); 
            
             return false;
    
    }

  }
  
  //PEGANDO O NOME DA AREA
  function getNomeArea($area_comum){
      $sqlValueArea = "SELECT de_cadastro_reserva_area_comum FROM cadastro_area_comum WHERE id_cadastro_reserva_area_comum = $area_comum";
    $resultValueArea = $this->getResultOnly($sqlValueArea);
    return  $resultValueArea[0];
      
  }
  
  //PEGANDO OS DADOS DO CONDOMINIO
 function getDadosCondominioSql(){

  $sqlDadosCondominio = "SELECT email_condominio  FROM condominio";
 
    $resultDadosCondo = $this->getResultOnly($sqlDadosCondominio);
    
    return  $resultDadosCondo;
} 

  function checkTimeForArea($id_area_comum, $hourStart, $hourEnd, $dt_data, $id_area_pai){
	
    $dia_semana = date("N", strtotime($dt_data));
    $horaInicio = date('H:i:s',strtotime($hourStart));
    $horaFim = date ('H:i:s',strtotime($hourEnd));
    $duracao = abs(strtotime($hourEnd)-(strtotime($hourStart)))/3600;
    
    //REGRA COVID
    //$duracaoCovid = abs(strtotime($hourEnd)-(strtotime($hourStart)))/60;


    $sqlValueHorario = "SELECT st_horario_sn FROM cadastro_area_comum WHERE id_cadastro_reserva_area_comum = $id_area_comum";
    $resultValueHorario = $this->getResultOnly($sqlValueHorario);
    

    $sqlValue = "SELECT hr_inicio, hr_fim, tmp_duracao FROM dia_reserva_area_comum WHERE num_dia = '$dia_semana' and id_area_comum = $id_area_comum";
    
    $resultValue = $this->getResultOnly($sqlValue);
    
     
    //Verificacao se pode mudar a data, se nao for possivel eh setado o valor do banco
    if ($resultValueHorario[0] == 'N') {
       $horaInicio = $resultValue[0];

    }
    


    $GLOBALS['validateTimeForArea'] = "";

    if($horaInicio < $resultValue[0]){
      $GLOBALS['validateTimeForArea'] = "Desculpe, a área só pode ser reservada a partir das ". substr($resultValue[0], 0, -3)." horas.";

    //}
    //else if($id_area_pai == 11 && $duracaoCovid > 10){//REGRA POR CAUSA DO COVID
     // $GLOBALS['validateTimeForArea'] = "Desculpe, a área só pode ser reservada teste durante 10 minutos.";

    }else if($horaFim > $resultValue[1]){
      $GLOBALS['validateTimeForArea'] = "Desculpe, a área só pode ser reservada até as ". substr($resultValue[1], 0, -3)." horas.";

    }else if($duracao > $resultValue[2]){
      $GLOBALS['validateTimeForArea'] = "Desculpe, a área só pode ser reservada durante". $resultValue[2]." hora(s).";

    }

    if(empty($GLOBALS['validateTimeForArea'])){
      return false;
    }else{
      return true;
    }


  }
 //FUNCAO QUE VERIFICA SE O IMOVEL ESTA NA TABELA DE BLLOQUEIO_PERIODO
  function checkImmobileBlock($id_numero_imovel, $id_bloco, $dt_data){

    $sqlCheckBlock = "SELECT id_bloco, id_numero_imovel FROM bloqueio_periodo 
                 WHERE id_bloco = $id_bloco AND id_numero_imovel = $id_numero_imovel
                 AND dt_fim >= '$dt_data'";



    $resultValue = $this->getResultOnly($sqlCheckBlock);
    if(!empty($resultValue[0])) {
      return true;
    }else{
      return false;
    }
  }


}
?>
