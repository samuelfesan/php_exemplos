<?php

namespace App\Jobs;

//Precisa importar a classe ServiceImport
use App\Traits\MainServiceApiTrait;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ImportData implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    use MainServiceApiTrait;

    public $tries = 2;

    protected $data;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $produtos = $this->montaParametrosProdutos($this->data);
        $inserirProdutos = $this->montarInsert('oc_product', $produtos);

        $produtosDescricao = $this->montaParametrosProdutosDescricao($this->data);
        $inserirProdutosDescricao = $this->montarInsert('oc_product_description', $produtos);

        $produtosCategoria = $this->montaParametrosProdutosCategoria($this->data);
        $inserirProdutosCategoria = $this->montarInsert('oc_product_to_category', $produtos);
        

        if (!isset($inserirProdutos) && empty($inserirProdutos)) {
            Log::info('Produtos.', ['message' => 'Nao existem produtos para serem importados']);
            return false;
        }

        try {
            DB::connection('virtus')->beginTransaction();

            $this->inserir('oc_product', $inserirProdutos);
            $this->inserir('oc_product_description', $inserirProdutosDescricao);
            $this->inserir('oc_product_to_category', $inserirProdutosCategoria);

            DB::connection('virtus')->commit();

            return true;

        } catch (\PDOException $exception) {
            DB::connection('virtus')->rollBack();
            Log::info('Falha na importação de Produtos.', ['message' => $exception]);
            return false;
        }
    }

    public function montarInsert($tabela, $data) 
    {
        $excluiColunasDaComparacaoUpdate = $this->recuperarRegras($tabela);
        
        $queryObject = (new QueryGenerateInsertOrUpdate)->generate($table, $data, $excludedColumnsFromUpdate);
        
        $getQuery = $queryObject->getQuery();
        
        $getObject = $queryObject->getBindings();

        return $getObject;
    }

    
    public function recuperarRegras($tabela) 
    {
        if ($tabela == 'oc_product') {
            $excluiColunasDaComparacaoUpdate = ['date_modified', 'date_added', 'quantity', 'date_available', 'weight']; 
        } else if ($tabela == 'oc_product_description') {
            $excluiColunasDaComparacaoUpdate = ['description', 'meta_title', 'meta_keyword'];
        }
        
        return $excluiColunasDaComparacaoUpdate;
    }

    public function inserir($tabela, $data)
    {
        if (isset($data) && !empty($data)) {
            DB::connection('virtus')->table($tabela)->insert($data);
        }
    }

    public function failed(\Exception $exception)
    {
        Log::info('Falha na importação de Produtos.', ['message' => $exception]);
        return false;
    }
}



