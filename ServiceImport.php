<?php
class ServiceImport
{
    public function montaParametrosProdutos($data)
    {
        $table = 'oc_product';

        collect($data)->each(function ($value) {

            $produtoId = $value->produto->id;
            $preco = (float)$value->precoVenda1;
            $weight = isset($value->produto->pesoBruto) && !empty($value->produto->pesoBruto) ? $value->produto->pesoBruto : 0;
            $length = isset($value->produto->comprimento) && !empty($value->produto->comprimento) ? $value->produto->comprimento : 0;
            $width = isset($value->produto->largura) && !empty($value->produto->largura) ? $value->produto->largura : 0;
            $height = isset($value->produto->altura) && !empty($value->produto->altura) ? $value->produto->altura : 0;

            $data = [
                [
                    'product_id' => $value->produto->id, 'quantity' => '0',
                    'price' => (float)$value->precoVenda1, 'date_available' => now(), 'weight' => $weight,
                    'length' => $length, 'width' => $width, 'heigth' => $height,
                    'status' => 1, 'date_added' => now(), 'date_modified' => now()
                ]
            ]; 
        }

        return $data;
    }

    public function montaParametrosProdutosDescricao($data)
    {
        $table = 'oc_product_description';

        collect($data)->each(function ($value) {

            $value->nome = isset($value->produto->descricao) ? $value->produto->descricao : '';
            $metaString= Str::slug($value->nome);

            $data = [
                [
                    'product_id' => $value->produto->id, 'name' => $value->nome, 'description' => $value->nome,
                    'tag' => '', 'meta_title' => $metaString, 'meta_description' => $metaString,
                    'language' => 2, 'meta_keyword' => $metaString
                ]
            ];
        }

        return $data;
    }

    public function montaParametrosProdutosCategoria($data)
    {
        $table = 'oc_product_to_category';
        
        collect($data)->each(function ($value) {

            $categoryResultId = $this->recuperaIdDaCategoria($value);
            
            if (isset($categoryResultId[0]->category_id)) {

                $categoryId = $categoryResultId[0]->category_id;

                $data = [
                    [
                        'product_id' => $value->produto->id, 'category_id' => $categoryId
                    ]
                ];
            }
        }

        return $data;
    }

    public function podeCadastrar($productId) 
    {
        $data = DB::connection('virtus')
        ->select("SELECT `product_id` FROM `oc_product` WHERE `product_id` = $id");

        return (isset($data) && !empty($data)) ? false : true;
    }

    private function recuperaIdVirtusDaCategoria($data)
    {
        $where = "WHERE ";

        foreach ($data as $value) {

            if (isset($value->subgrupoId) && !empty($value->subgrupoId)) {
                $where .= "subgrupoId = {$value->subgrupoId}";
            } else {
                $where .= "subgrupoId is null";
            }

            if (isset($value->grupoId) && !empty($value->grupoId)) {
                $where .= " AND grupoId = {$value->grupoId}";
            } else {
                $where .= " AND grupoId is null";
            }

            if (isset($value->secaoId) && !empty($value->secaoId)) {
                $where .= " AND secaoId = {$value->secaoId}";
            } else {
                $where .= " AND secaoId is null";
            }
        }

        return DB::connection('virtus')->select("SELECT category_id FROM oc_category " . $where);
    }
}