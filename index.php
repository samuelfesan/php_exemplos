<?php
require_once 'QueryGenerateInsertOrUpdate.php';
require_once 'QueryObject.php';

$table = 'users';

$data = [
    ['secaoId' => 1, 'grupoId' => '', 'subgrupoId' => ''],
    ['secaoId' => 1, 'grupoId' => 2, 'subgrupoId' => ''],
    ['secaoId' => 1, 'grupoId' => 2, 'subgrupoId' => 3]
];

/*collect($data->subGrupos)->each(function ($value) use (&$valuesReplaceCategory, &$valuesReplaceCategoryPath) {
    $categoryIdGrupo = $this->getCategoryIdByRecord($value->grupoId, 'grupoId');
    $categoryIdSection = $this->getCategoryIdByRecord($value->secaoId, 'secaoId');

    $valuesSubGroups =  ['subgrupoId' => $value->id, 'grupoId' => $value->grupoId, 'secaoId' => $value->secaoId, 'parent_id' => $categoryIdGrupo, 'top' => 0, 'column' => 0,
        'sort_order' => 0, 'status' => 1, 'date_added' => NOW(), 'date_modified' => NOW()];

    $categoryModel = Category::updateOrCreate(['subgrupoId' => $value->id], $valuesSubGroups);
    $description = "'".$value->descricao."'";

    $valuesReplaceCategory .= "($categoryModel->category_id, $description, $description, $description, $description, $description),";
    $valuesReplaceCategoryPath .= "($categoryModel->category_id, $categoryIdSection, 0),";
    $valuesReplaceCategoryPath .= "($categoryModel->category_id, $categoryIdGrupo, 1),";
    $valuesReplaceCategoryPath .= "($categoryModel->category_id, $categoryModel->category_id, 2),";

});
*/


$excludedColumnsFromUpdate = ['date_modified', 'date_added', 'top', 'column', 'sort_order', 'status'];

$queryObject = (new QueryGenerateInsertOrUpdate)->generate($table, $data, $excludedColumnsFromUpdate);

$getQuery = $queryObject->getQuery();
print_r($getQuery);
$getObject = $queryObject->getBindings();

